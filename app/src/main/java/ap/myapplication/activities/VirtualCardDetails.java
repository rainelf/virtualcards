package ap.myapplication.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;

import ap.myapplication.R;
import databases.VirtualCardsDBHandler;
import models.VirtualCard;

public class VirtualCardDetails extends AppCompatActivity {
    VirtualCardsDBHandler dbHandler;
    Bitmap thumbnail;
    private static final String TAG = VirtualCardDetails.class.getSimpleName();

    private File imgFile = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_virtual_card_details);

        dbHandler = new VirtualCardsDBHandler(this, null, null, 1);

        Intent intent = getIntent();
        String vcName = intent.getStringExtra("vcName");
        VirtualCard vc = dbHandler.getVirtualCardByName(vcName);

        Log.i(TAG, "onCreate: "+ vc.getName() + "|" + vc.getNumber());

        TextView name = (TextView) findViewById(R.id.nameValue);
        name.setText(vc.getName());

        TextView owner = (TextView) findViewById(R.id.ownerValue);
        owner.setText(vc.getOwner());

        TextView number = (TextView) findViewById(R.id.numberValue);
        number.setText(vc.getNumber());

        TextView url = (TextView) findViewById(R.id.urlValue);
        url.setText(vc.getUrl());

        getCardImage(vc.getImageUri());

    }

    private void getCardImage(String imageName) {
        try {
            imgFile = new  File(imageName);
            Bitmap thumbnail = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

            ImageView imageView = (ImageView) findViewById(R.id.cardImageValue);
            imageView.setImageBitmap(thumbnail);

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(VirtualCardDetails.this, Image.class);
                    intent.putExtra("imageUri", imgFile.getAbsolutePath());
                    startActivity(intent);
                }
            });

        } catch (Exception e) {
            Log.e(TAG, "getCardImage: " +  e.toString() );
        }
    }
}
