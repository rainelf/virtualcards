package ap.myapplication.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;

import java.util.ArrayList;

import ap.myapplication.R;
import databases.VirtualCardsDBHandler;
import models.VirtualCard;

public class ListCards extends AppCompatActivity {

    private static final String TAG = ListCards.class.getSimpleName();
    VirtualCardsDBHandler dbHandler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_cards);

        dbHandler = new VirtualCardsDBHandler(this, null, null, 1);

        ImageButton addCard = (ImageButton) findViewById(R.id.addCard);

        addCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListCards.this, AddCard.class);
                startActivity(intent);
            }
        });



        ListView cardListView = (ListView)  findViewById(R.id.listViewCards);
        ArrayList<String> listViewNames = this.getVirtualCards(dbHandler);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, listViewNames);
        cardListView.setAdapter(adapter);

        cardListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String value = (String) parent.getItemAtPosition(position);
                Intent intent = new Intent(ListCards.this, VirtualCardDetails.class);
                intent.putExtra("vcName", value);
                startActivity(intent);


            }
        });

    }

    private ArrayList<String> getVirtualCards(VirtualCardsDBHandler dbHandler)
    {
        ArrayList<String> vcs = new ArrayList<String>();
        ArrayList<VirtualCard> virtualCardsObjects = dbHandler.getVirtualCards();
        for (int i = 0; i < virtualCardsObjects.size(); i++) {
            vcs.add(virtualCardsObjects.get(i).getName());
        }
        return vcs;
    }
}
