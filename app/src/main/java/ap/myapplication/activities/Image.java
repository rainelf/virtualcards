package ap.myapplication.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ZoomControls;

import java.io.File;

import ap.myapplication.R;

public class Image extends AppCompatActivity {

    ZoomControls zoomIt;
    ImageView iv;
    float mx = 0, my = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        Intent intent = getIntent();
        String vcImageUri = intent.getStringExtra("imageUri");

        File imgFile = new File(vcImageUri);
        Bitmap thumbnail = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

        iv = (ImageView) findViewById(R.id.fullImageView);
        iv.setImageBitmap(thumbnail);


        zoomIt = (ZoomControls) findViewById(R.id.zoomControls);

        zoomIt.setOnZoomInClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float x = iv.getScaleX();
                float y = iv.getScaleY();

                iv.setScaleX((int)x+1);
                iv.setScaleY((int)y+1);
            }
        });

        zoomIt.setOnZoomOutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float x = iv.getScaleX();
                float y = iv.getScaleY();

                if (x == 1 && y == 1)
                    return;

                iv.setScaleX((int)x-1);
                iv.setScaleY((int)y-1);
            }
        });

        iv.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View arg0, MotionEvent event) {

                float curX, curY;
                switch (event.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        mx = event.getX();
                        my = event.getY();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        curX = event.getX();
                        curY = event.getY();
                        iv.scrollBy((int) (mx - curX), (int) (my - curY));
                        mx = curX;
                        my = curY;
                        break;
                    case MotionEvent.ACTION_UP:
                        curX = event.getX();
                        curY = event.getY();
                        iv.scrollBy((int) (mx - curX), (int) (my - curY));
                        break;
                }

                return true;
            }
        });

    }
}
