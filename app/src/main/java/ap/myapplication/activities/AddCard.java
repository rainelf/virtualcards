package ap.myapplication.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.LoginFilter;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import ap.myapplication.R;
import databases.VirtualCardsDBHandler;
import models.VirtualCard;

public class AddCard extends AppCompatActivity {

    private static final String TAG = AddCard.class.getSimpleName();
    VirtualCardsDBHandler dbHandler;

    private ImageView mImageView;
    private Bitmap cardImage = null;

    private static final int MULTIPLE_ACCESS_REQUEST_CODE = 1;
    // your authority, must be the same as in your manifest file
    private static final String CAPTURE_IMAGE_FILE_PROVIDER = "ap.myapplication.fileprovider";
    String imageFileName;

    String mCurrentPhotoPath;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_card);


        Button cancelCard = (Button) findViewById(R.id.btnCancel);

        cancelCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddCard.this, ListCards.class);
                startActivity(intent);
            }
        });

        Button addCard = (Button) findViewById(R.id.btnSubmit);
        addCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addCardToDatabase();
            }
        });

        mImageView = (ImageView) findViewById(R.id.cardThumbNail);


        Button takePicture = (Button) findViewById(R.id.btnTakePicture);
        takePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Check permission for CAMERA
                if (ActivityCompat.checkSelfPermission(AddCard.this, Manifest.permission.CAMERA)                              != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(AddCard.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)      != PackageManager.PERMISSION_GRANTED) {
                    // Check Permissions Now
                    // Callback onRequestPermissionsResult interceptado na Activity MainActivity
                    ActivityCompat.requestPermissions(AddCard.this, new String[]{Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE},AddCard.MULTIPLE_ACCESS_REQUEST_CODE );

                } else {
                    // permission has been granted, continue as usual

                    dispatchTakePictureIntent();
                }


            }
        });

        dbHandler = new VirtualCardsDBHandler(this, null, null, 1);
    }

    private void addCardToDatabase()
    {
        EditText name = (EditText) findViewById(R.id.name);
        String nameValue = name.getText().toString();

        EditText owner = (EditText) findViewById(R.id.owner);
        String ownerValue = owner.getText().toString();

        EditText number = (EditText) findViewById(R.id.number);
        String numberValue = number.getText().toString();

        EditText url = (EditText) findViewById(R.id.url);
        String urlValue = url.getText().toString();

//        EditText name = (EditText) findViewById(R.id.name);
        String imageUriValue = "/";

        VirtualCard vc = new VirtualCard();
        vc.setName(nameValue);
        vc.setOwner(ownerValue);
        vc.setNumber(numberValue);
        vc.setUrl(urlValue);

        Log.i(TAG, "addCardToDatabase: "+ dbHandler.databaseToString());

        //save image to storage

        vc.setImageUri(this.imageFileName);

        dbHandler.addVirtualCard(vc);

        // Redirect to list cards
        Intent intent = new Intent(AddCard.this, ListCards.class);
        startActivity(intent);


    }



//    private void dispatchTakePictureIntent()  {
//        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
//        imageFileName = "CARD_IMAGE_" + timeStamp + ".jpg";
//        try {
//            File tempFile = new File(this.getFilesDir().toString(), imageFileName);
//            imageFileName = this.getFilesDir().toString() + "/" + imageFileName;
//
//            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//            Bundle mBundle = new Bundle();
//            mBundle.putString("externalImageUri", imageFileName);
//            intent.putExtras(mBundle);
//
//            startActivityForResult(intent, this.MULTIPLE_ACCESS_REQUEST_CODE);
//        } catch (Exception e) {
//            Log.i(TAG, "dispatchTakePictureIntent: ", e);
//        }
//    }

    private void dispatchTakePictureIntent() {
        File path = new File(getFilesDir(), "vc_images");
        if (!path.exists()) path.mkdirs();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        this.imageFileName = "CARD_IMAGE_" + timeStamp + ".jpg";
        File image = new File(path, this.imageFileName);
        Uri imageUri = FileProvider.getUriForFile(AddCard.this, CAPTURE_IMAGE_FILE_PROVIDER, image);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intent, MULTIPLE_ACCESS_REQUEST_CODE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == MULTIPLE_ACCESS_REQUEST_CODE) {
            if (resultCode == this.RESULT_OK) {
                try {
                    File path = new File(getFilesDir(), "vc_images");
                    if (!path.exists()) path.mkdirs();
                    File imageFile = new File(path, this.imageFileName);
                    Bitmap thumbnail = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
                    ImageView iv = (ImageView) findViewById(R.id.cardThumbNail);
                    iv.setImageBitmap(thumbnail);
                    imageFileName = imageFile.getAbsolutePath();

                } catch (Exception e) {

                }

            }
        }
        super.onActivityResult(requestCode, resultCode, intent);
    }


//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
//        if (requestCode == MULTIPLE_ACCESS_REQUEST_CODE) {
//            if (resultCode == this.RESULT_OK) {
//                Bundle extras = intent.getExtras();
//                Bitmap thumbnail = (Bitmap) extras.get("data");
//
//                File thumbnail2File =  new File(intent.getData().getPath().toString());
//                Bitmap thumbnail2 = BitmapFactory.decodeFile(thumbnail2File.getAbsolutePath());
//
//                ImageView imageView = (ImageView) findViewById(R.id.cardThumbNail);
//                imageView.setImageBitmap(thumbnail2);
//
//
//                OutputStream stream = null;
//                try {
//                    String imageFileName = intent.getStringExtra("externalImageUri");
//                    stream = new FileOutputStream(this.imageFileName);
//                    /* Write bitmap to file using JPEG and 80% quality hint for JPEG. */
//                    thumbnail.compress(Bitmap.CompressFormat.JPEG, 80, stream);
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//        super.onActivityResult(requestCode, resultCode, intent);
//    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MULTIPLE_ACCESS_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Toast.makeText(getApplicationContext(), "Permission granted", Toast.LENGTH_SHORT).show();
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }



}
