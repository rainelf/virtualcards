package databases;

import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.Cursor;
import android.content.Context;
import android.content.ContentValues;
import android.util.Log;

import java.util.ArrayList;

import models.VirtualCard;

public class VirtualCardsDBHandler extends SQLiteOpenHelper {

    public static final String TAG = VirtualCardsDBHandler.class.getSimpleName();
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "virtual_cards.db";
    public static final String TABLE_VIRTUAL_CARDS = "virtual_cards";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_OWNER = "owner";
    public static final String COLUMN_NUMBER = "number";
    public static final String COLUMN_URL = "url";
    public static final String COLUMN_IMAGE_URI = "image_uri";



    public VirtualCardsDBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE " + TABLE_VIRTUAL_CARDS + "(" +
                COLUMN_ID           + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                COLUMN_NAME         + " TEXT," +
                COLUMN_OWNER       + " TEXT," +
                COLUMN_NUMBER        + " TEXT," +
                COLUMN_URL         + " TEXT," +
                COLUMN_IMAGE_URI + " TEXT" +

                ");";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_VIRTUAL_CARDS + ";");
        onCreate(db);
    }

    //Add a new row to the database
    public void addVirtualCard (VirtualCard vc) {
        try {
            ContentValues values = new ContentValues();
            values.put(COLUMN_NAME, vc.getName());
            values.put(COLUMN_OWNER, vc.getOwner());
            values.put(COLUMN_NUMBER, vc.getNumber());
            values.put(COLUMN_URL, vc.getUrl());
            values.put(COLUMN_IMAGE_URI, vc.getImageUri());

            SQLiteDatabase db = getWritableDatabase();
            long id = db.insert(TABLE_VIRTUAL_CARDS, null, values);
            Log.i(TAG, "addSong: inserted " + id + ", name:" +  vc.getName() + "["+vc.getOwner()+"]");
            db.close();
        } catch (Exception e) {
            Log.i(TAG, "addSong: exception" + e.toString());
        }
    }

    //Print the database as a string
    public String databaseToString () {
        String dbString = "";
        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_VIRTUAL_CARDS + " WHERE 1";

        Log.i(TAG, "databaseToString: ");

        // Cursor points to a location in your results
        Cursor c = db.rawQuery(query, null);
        // Move to the first row
        c.moveToFirst();
        while (!c.isAfterLast()) {

            if (c.getString(c.getColumnIndex(COLUMN_NAME))!= null) {
                dbString += c.getString(c.getColumnIndex(COLUMN_NAME));
            }
            if (c.getString(c.getColumnIndex(COLUMN_OWNER))!= null) {
                dbString += " " + c.getString(c.getColumnIndex(COLUMN_OWNER));
            }
            if (c.getString(c.getColumnIndex(COLUMN_NUMBER))!= null) {
                dbString += " " +  c.getString(c.getColumnIndex(COLUMN_NUMBER));
            }
            if (c.getString(c.getColumnIndex(COLUMN_URL))!= null) {
                dbString += " " +  c.getString(c.getColumnIndex(COLUMN_URL));
            }
            dbString += "\n";
            c.moveToNext();
        }
        Log.i(TAG, "databaseToString: virtual card list" +  dbString);
        long cnt  = DatabaseUtils.queryNumEntries(db, TABLE_VIRTUAL_CARDS);
        Log.i(TAG, "databaseToString: Total of " +  cnt + " virtual cards.");
        db.close();
        return dbString;
    }

    public ArrayList<VirtualCard> getVirtualCards ()
    {
        ArrayList<VirtualCard> vcs = new ArrayList<VirtualCard>();
        SQLiteDatabase db = getWritableDatabase();

        String query = "SELECT * FROM " + TABLE_VIRTUAL_CARDS + " WHERE 1";
        // Cursor points to a location in your results
        Cursor c = db.rawQuery(query, null);
        // Move to the first row
        c.moveToFirst();
        while (!c.isAfterLast()) {
            if (c.getString(c.getColumnIndex(COLUMN_ID))!= null) {
                VirtualCard vc = new VirtualCard();
                vc.setName(c.getString(c.getColumnIndex(COLUMN_NAME)));
                vc.setOwner(c.getString(c.getColumnIndex(COLUMN_OWNER)));
                vc.setNumber(c.getString(c.getColumnIndex(COLUMN_NUMBER)));
                vc.setUrl(c.getString(c.getColumnIndex(COLUMN_URL)));
                vc.setImageUri(c.getString(c.getColumnIndex(COLUMN_IMAGE_URI)));
                vcs.add(vc);
            }
            c.moveToNext();
        }
        /* return the list of virtual cards - vcs */
        return vcs;

    }


    public void emptyDatabase()
    {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM '" +  TABLE_VIRTUAL_CARDS + "' WHERE 1");
        db.close();
    }

    public VirtualCard getVirtualCardByName (String name)
    {
        VirtualCard vc = new VirtualCard();
        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_VIRTUAL_CARDS + " WHERE " + COLUMN_NAME + " = '" + name + "'";

        // Cursor points to a location in your results
        Cursor c = db.rawQuery(query, null);
        // Move to the first row
        c.moveToFirst();

        if (c.getString(c.getColumnIndex(COLUMN_ID))!= null) {
            vc.setId(c.getInt(c.getColumnIndex(COLUMN_ID)));
            vc.setName(c.getString(c.getColumnIndex(COLUMN_NAME)));
            vc.setOwner(c.getString(c.getColumnIndex(COLUMN_OWNER)));
            vc.setNumber(c.getString(c.getColumnIndex(COLUMN_NUMBER)));
            vc.setUrl(c.getString(c.getColumnIndex(COLUMN_URL)));
            vc.setImageUri(c.getString(c.getColumnIndex(COLUMN_IMAGE_URI)));
        }
        Log.i(TAG,
                "getVirtualCardByName: "+ c.getInt(c.getColumnIndex(COLUMN_ID)) + " " +
                        c.getString(c.getColumnIndex(COLUMN_NAME)) + " " +
                        c.getString(c.getColumnIndex(COLUMN_OWNER)) + " " +
                        c.getString(c.getColumnIndex(COLUMN_NUMBER)) + " " +
                        c.getString(c.getColumnIndex(COLUMN_URL)) + " "

        );
        return vc;
    }

}
